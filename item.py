class Item:
    def __init__(self, name, description):
        self.name = name
        self.description = description

items = {
    "health potion": Item("Health Potion", "A magical drink that heals the body when consumed."),
    "purple drank": Item("Purple Drank", "You're not sure what it is, but it's purple, and people drink it."),
    "rock": Item("Rock", "A small stone."),
}
