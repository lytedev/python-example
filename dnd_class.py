class DNDClass:
    def __init__(self, name, description, primary_attribute):
        self.name = name
        self.description = description
        self.primary_attribute = primary_attribute

dnd_classes = {
    "fighter": DNDClass("Fighter", "Use swords and brute strength to beat your way through challenges.", "Strength"),
    "ranger": DNDClass("Ranger", "Pick off your foes from afar.", "Dexterity"),
    "rogue": DNDClass("Rogue", "Kill and steal while never being seen or heard.", "Dexterity"),
    "wizard": DNDClass("Wizard", "Employ dangerous, powerful magics to do your bidding.", "Intelligence")
}
