class Race:
    def __init__(self, name, str_mod, int_mod):
        self.name = name
        self.str_mod = str_mod
        self.int_mod = int_mod

races = {
    "elf": Race("Elf", -1, 2),
    "human": Race("Human", 1, 1),
    "dwarf": Race("Dwarf", 2, -1),
}
