from pprint import pprint

from player import Player
from race import races
from dnd_class import dnd_classes
from item import items

import weapon # make sure the weapons get added to the items dict

name = input("What is your character's name? ")
race = races[input("What is your Race? ")]
dnd_class = dnd_classes[input("What is your DND Class? ")]
item = items[input("What is your primary item? ")]

player = Player(name, race, dnd_class, item)

pprint(vars(player))
pprint(vars(player.race))
pprint(vars(player.dnd_class))
pprint(vars(player.weapon))
