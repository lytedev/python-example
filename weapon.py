import item

class Weapon(item.Item):
    def __init__(self, name, description, damage):
        super().__init__(name, description) # this calls the parent class's (item.Item) constructor
        self.damage = damage

# add to the items dict retroactively
item.items["shortsword"] = Weapon("Shortsword", "A short sword.", 6)
item.items["longsword"] = Weapon("Longsword", "A long, looooooong sword. Overcompensating?", 8)
item.items["buster sword"] = Weapon("Buster Sword", "Seriously. You can't even lift it.", 9001)
item.items["dagger"] = Weapon("Dagger", "A short knife for killing swiftly and silently. Shhhhh...", 4)
